package CharityLandEJB.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Rubrique implements Serializable{
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private int id;
	private String type;
	@OneToOne
	private Acte acteId;
	public Rubrique() {
		super();
	}
	
	public Acte getActeId() {
		return acteId;
	}

	public void setActeId(Acte acteId) {
		this.acteId = acteId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Rubrique(int id, String type, Acte acteId) {
		super();
		this.id = id;
		this.type = type;
		this.acteId = acteId;
	}

	@Override
	public String toString() {
		return "Rubrique [id=" + id + ", type=" + type + ", acteId=" + acteId + "]";
	}
	

	
	

}
