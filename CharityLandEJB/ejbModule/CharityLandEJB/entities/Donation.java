package CharityLandEJB.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class Donation implements Serializable{
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private int id;
	@Temporal(TemporalType.DATE)
	private Date dateCreation;
	@ManyToOne
	private Utilisateur donateurId;
	@ManyToOne
	private Acte acteId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	public Acte getActeId() {
		return acteId;
	}
	public void setActeId(Acte acteId) {
		this.acteId = acteId;
	}
	
	public Utilisateur getDonateurId() {
		return donateurId;
	}
	public void setDonateurId(Utilisateur donateurId) {
		this.donateurId = donateurId;
	}
	public Donation() {
		super();
	}
	@Override
	public String toString() {
		return "Donation [id=" + id + ", dateCreation=" + dateCreation + ", donateurId=" + donateurId + ", acteId="
				+ acteId + "]";
	}
	
	

}
