package CharityLandEJB.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


public class Association extends Utilisateur implements Serializable{
	
	private String titre;
	private String adresse;
	private String description;
	private String papierVerifier;
	private Boolean estverifier;

	public Association() {
		super();
	}

	
	public String getPapierVerifier() {
		return papierVerifier;
	}


	public void setPapierVerifier(String papierVerifier) {
		this.papierVerifier = papierVerifier;
	}


	public Boolean getEstverifier() {
		return estverifier;
	}


	public void setEstverifier(Boolean estverifier) {
		this.estverifier = estverifier;
	}


	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}



	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public Association(int cid,String mail,String password,String role,Photo photoId,String phone,String titre, String adresse, String description, String papierVerifier, Boolean estverifier) {
		this.titre = titre;
		this.adresse = adresse;
		this.description = description;
		this.papierVerifier = papierVerifier;
		this.estverifier = estverifier;
	}


	@Override
	public String toString() {
		return "Association ["+super.toString()+"titre=" + titre + ", adresse=" + adresse + ", description=" + description
				+ ", papierVerifier=" + papierVerifier + ", estverifier=" + estverifier + "]";
	}


	
	
	
	

	

}
