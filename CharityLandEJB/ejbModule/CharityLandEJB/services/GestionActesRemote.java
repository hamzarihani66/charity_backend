package CharityLandEJB.services;

import java.util.List;

import javax.ejb.Remote;

import CharityLandEJB.entities.Acte;
import CharityLandEJB.entities.Utilisateur;

@Remote
public interface GestionActesRemote {

	void ajouterActe(Acte acte);

	void updateActe(Acte acte);

	Acte findActeByCid(int id);

	void deleteActe(Acte acte);

	List<Acte> findAllActes();

}
