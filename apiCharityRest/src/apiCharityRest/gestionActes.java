package apiCharityRest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import CharityLandEJB.entities.Acte;
import CharityLandEJB.services.GestionActesRemote;

@Path("/")
public class gestionActes {
	@EJB
	GestionActesRemote gestionActesRemote;
	Acte acte = new Acte();
	List<Acte> actes = new ArrayList<Acte>();

	@PostConstruct
	public void init() {
		actes = gestionActesRemote.findAllActes();
		System.out.println(actes);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/actes")
	public Response getActes() {
		return Response.status(Status.ACCEPTED).entity(this.actes).header("Access-Control-Allow-Origin", "*").allow("OPTIONS").build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/actes/{cid}")
	public Response getActeById(@PathParam("cid") int cid) {
			
		Acte findacte=gestionActesRemote.findActeByCid(cid);
		return Response.status(Status.ACCEPTED).entity(findacte).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/actes")
	public Response addActe(Acte acte) {
		gestionActesRemote.ajouterActe(acte);
		return Response.status(Status.ACCEPTED).entity("acte ajouter avec success !!").build();
	}
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/actes/{cid}")
	public Response updateActe(@PathParam("cid") int cid, Acte act) {
		gestionActesRemote.updateActe(act);
		return Response.status(Status.ACCEPTED).entity("modifier avec success !!").build();
	}

	@DELETE
	@Path("/actes/{cid}")
	public Response deleteActe(@PathParam("cid") int cid) {
		Acte act=gestionActesRemote.findActeByCid(cid);
		gestionActesRemote.deleteActe(act);
		return Response.status(Status.ACCEPTED).entity("supprimer avec success !!").build();
	}

}
